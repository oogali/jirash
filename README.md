* jirash - an attempt a better jira cli

I love JIRA.  I also do a lot of work via the CLI. These two worlds collide.

Often.

I find it highly annoying to switch back and forth between iTerm and a browser
to enter data into, or retrieve data from a ticket.

But, I still need to get things done. So how?

My first attempt at a JIRA command line was during my days at Currenex, I
wrote a behemoth of a shell script that used cat, script, and curl to push
data up to JIRA. It was 'cute'.

My second attempt wasn't so much CLI-based, but instead a GUI. I tried
ALMWorks' JIRA Client (Java GUI), but the responsiveness is not quite
there. Plus, it fails often on performing tasks on multiple tickets.

My third attempt was with Ruby, SOAP, and the jira4r gem. Are you cringing?

I still am.

So here we are, attempt #4.

I have several status calls each day with multiple teams in multiple orgs,
and from each call a plethora of information needs to be captured, chopped up,
and delegated.

I no longer want to be the guy on the conference call asking for information
twice because my input interface is too slow.

The requirements:
- command-line only
- must be blazing fast (asynchronous?)
- must interface with multiple JIRA instances (not at the same time)
- create, assign, link, resolve, close, move functionality
